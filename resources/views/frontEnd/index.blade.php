@extends('frontEnd.layouts.master')
@section('title','Home Page')
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    @include('frontEnd.layouts.category_menu')
                </div>

                <div class="col-sm-9 padding-right">
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">Sản phẩm bán chạy</h2>
                        @if(count($productHot) > 0)
                        <div class="recommended_items"><!--recommended_items-->
                            <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
                                <div class="carousel-inner">
                                    <?php $countChunk = 0;?>
                                    @foreach($productHot->chunk(3) as $chunk)
                                        <?php $countChunk++; ?>
                                        <div class="item<?php if($countChunk==1){ echo' active';} ?>">
                                            @foreach($chunk as $item)
                                                <div class="col-sm-4">
                                                    <div class="product-image-wrapper">
                                                        <div class="single-products">
                                                            <div class="productinfo text-center">
                                                                <a href="{{url('/product-detail',$item->id)}}">
                                                                    <img src="{{url('/products/small',$item->image)}}" alt="" style="width: 150px;"/>
                                                                </a>
                                                                <h2>{{$item->price}} VND</h2>
                                                                <a href="{{url('/product-detail',$item->id)}}">
                                                                    <h5>{{$item->p_name}}</h5>
                                                                </a>
                                                                <button type="button" class="btn btn-default add-to-cart"><i class="fa fa-shopping-cart"></i>Add to cart</button>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            @endforeach
                                        </div>
                                    @endforeach
                                </div>
                                <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                                    <i class="fa fa-angle-left"></i>
                                </a>
                                <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                                    <i class="fa fa-angle-right"></i>
                                </a>
                            </div>
                        </div><!--/recommended_items-->
                        @endif
                    </div>
                    <div class="features_items"><!--features_items-->
                        <h2 class="title text-center">Danh sách sản phẩm</h2>
                        @foreach($products as $product)
                            @if($product->category->status==1)
                                <div class="col-sm-4">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                <a href="{{url('/product-detail',$product->id)}}">
                                                    <div class="img">
                                                    <img src="{{url('products/small/',$product->image)}}" alt="" />
                                                        <div class="txt">
                                                            <h3 class="title_product">{{$product->p_name}}</h3>
                                                            <button href="{{url('/product-detail',$product->id)}}" class="btn btn-warning">Chi tiết</button>
                                                        </div>
                                                    </div>
                                                </a>
                                                <h2>{{$product->price}} VND</h2>
                                                <a href="{{url('/product-detail',$product->id)}}">
                                                    <h4>{{$product->p_name}}</h4>
                                                </a>
                                            </div>
                                        </div>
                                        <div class="choose">
                                            <ul class="nav nav-pills nav-justified">
                                                <li><a href="#"><i class="fa fa-heart"></i>Add to wishlist</a></li>
                                                <li><a href="#"><i class="fa fa-plus-square"></i>Add cart</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        @endforeach
                    </div>
                    <div class="text-center">{{ $products->links() }}</div>
                </div>
            </div>
        </div>
    </section>
@endsection