@extends('frontEnd.layouts.master')
@section('title','List Products')
@section('slider')
@endsection
@section('content')
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                @include('frontEnd.layouts.category_menu')
            </div>
            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    <?php
                            if($byCate!=""){
                                $products = $list_product;
                                echo '<h2 class="title text-center">Danh mục '.$byCate->name.'</h2>';
                            }else{
                                echo '<h2 class="title text-center">List Products</h2>';
                            }
                    ?>
                    @foreach($products as $product)
                        @if($product->category->status==1)
                        <div class="col-sm-4">
                            <div class="product-image-wrapper">
                                <div class="single-products">
                                    <div class="productinfo text-center">
                                        <a href="{{url('/product-detail',$product->id)}}">
                                            <div class="img">
                                            <img src="{{url('products/small/',$product->image)}}" alt="" />
                                                <div class="txt">
                                                    <h3 class="title_product">{{$product->p_name}}</h3>
                                                    <button href="{{url('/product-detail',$product->id)}}" class="btn btn-warning">Chi tiết</button>
                                                </div>
                                            </div>
                                        </a>
                                        <h2>{{$product->price}} VND</h2>
                                        <a href="{{url('/product-detail',$product->id)}}">
                                            <h4>{{$product->p_name}}</h4>
                                        </a>
                                    </div>
                                </div>
                                <div class="choose">
                                    <ul class="nav nav-pills nav-justified">
                                        <li><a href="#"><i class="fa fa-heart"></i>Add to wishlist</a></li>
                                        <li><a href="#"><i class="fa fa-plus-square"></i>Add cart</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        @endif
                    @endforeach
                </div><!--features_items-->
                <div class="text-center">{{ $products->links() }}</div>
            </div>
        </div>
    </div>
@endsection